//
//  EmpTableViewCell.swift
//  Firebase_Emp
//
//  Created by Tam Nguyen Dang Thien on 9/28/18.
//  Copyright © 2018 Tam Nguyen Dang Thien. All rights reserved.
//

import UIKit

class EmpTableViewCell: UITableViewCell {

    @IBOutlet weak var cellName: UILabel!
    
    @IBOutlet weak var cellPosition: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  Employee.swift
//  Firebase_Emp
//
//  Created by Tam Nguyen Dang Thien on 9/28/18.
//  Copyright © 2018 Tam Nguyen Dang Thien. All rights reserved.
//
import Foundation
struct EmployeeList : Decodable {
    let employees : [Employee]
}

struct Employee : Decodable {
    var name : String?
    var position : String?
    var team : String?
    var photo : String?
    let skills : [Experience]
    
    
}

struct Experience : Decodable {
    var name : String?
    var experience : String?
}
